#!/bin/bash

# The directory where the submodule will be cloned, relative to this script
SDK_DIR=sdk

# Stop if there's an error
set -e

# The patch for tsconfig
PATCH=$(
cat <<'EOF'
diff --git a/tsconfig.json b/tsconfig.json
index 646ad50..356cc16 100644
--- a/tsconfig.json
+++ b/tsconfig.json
@@ -1,7 +1,7 @@
 {
   "compilerOptions": {
     "target": "ES2020",
-    "module": "CommonJS",
+    "module": "ES2022",
     "moduleResolution": "node",
     "declaration": true,
     "strict": true,
EOF
)

# CD to the directory containing this script
cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null

# If the submodule doesn't exist, create it
if [ ! -e "$SDK_DIR" ]; then
    printf '\e[1;34mCloning the SDK as a submodule\e[0m\n'
    git submodule add --branch main https://gitlab.com/accumulatenetwork/sdk/javascript.git "$SDK_DIR"
    echo
fi

# CD to the submodule
cd "$SDK_DIR"

# Verify that it's on the main branch
if [ $(git rev-parse --abbrev-ref HEAD) != "main" ]; then
    >&2 printf '\e[1;31mNot on the main branch, aborting to avoid overwriting your changes\e[0m\n'
    exit 1
fi

# Revert the patch and pull changes
printf '\e[1;34mPulling SDK updates\e[0m\n'
git apply -R <(cat <<< "$PATCH") 2> /dev/null || true # ignore errors
git pull
echo

# Reapply the patch
printf '\e[1;34mPatching tsconfig for pure JS\e[0m\n'
git apply <(cat <<< "$PATCH")
echo

# Install dependencies and build
printf '\e[1;34mCompiling TS to JS\e[0m\n'
yarn install --ignore-scripts
yarn build
echo
