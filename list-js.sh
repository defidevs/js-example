#!/bin/bash

function A {
    for x in $(find sdk/lib -name 'index.js'); do
        echo '"/'${x%index.js}'":    { ".": "/'${x}'"},'
    done
}

function B {
    for x in $(find sdk/lib -name '*.js' -o -type d | sed 's/\.js$//' | sort); do
        if [[ -d "$x" ]]; then
            echo '"/'${x}'": "/'${x}/index.js'",'
        else
            echo '"/'${x}'": "/'${x}'.js",'
        fi
    done
}

x="$(B)"
echo "{$(A) \"/sdk/\": {${x%,}}}" | jq -M --indent 4