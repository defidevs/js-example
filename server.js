const express = require('express')
const fs = require('node:fs/promises')
const path = require('node:path')
const app = express()
const port = 3000

app.get(/.*/, async (req, res) => {
    for (const filepath of pathsFor(req)) {
        if (await exists(filepath)) {
            await sendFile(req, res, filepath);
            return;
        }
    }

    res.status(404);
    res.write(`404 not found: ${req.originalUrl}`);
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})

async function sendFile(req, res, filepath) {
    if (!('umd' in req.query)) {
        res.sendFile(filepath);
        return;
    }

    try {
        const data = await fs.readFile(filepath);
        const src = data.toString('utf-8').replace(/([\n;]\s*)(?:var|const|let)\s+(\w+)\s*=\s*require\(['"]([^'"]*)['"]\)([;\n])/g, (...m) => {
            if (m[3].startsWith('./') || m[3].startsWith('../')) {
                m[3] += '?umd'
            }
            return `${m[1]}import * as ${m[2]} from "${m[3]}"${m[4]}`;
        });

        res.type('.js');
        res.send(`export const exports = {};\n${src}`)
    } catch (error) {
        res.status(404);
        res.write(`404 not found: ${req.originalUrl}`);
    }
}

function pathsFor(req) {
    const p = req.originalUrl.replace(/\?.*/, '');
    if (p == '/') {
        return [path.join(__dirname, 'web', 'index.html')];
    }

    if (!p.startsWith('/sdk')) {
        return [path.join(__dirname, 'web', p)];
    }

    if (path.extname(p) != '') {
        return [path.join(__dirname, p)];
    }

    // This is a hack to work around imports. It can be removed if
    // web/index.html is updated to include all of the SDK's imports.
    return [
        path.join(__dirname, p),
        path.join(__dirname, p+'.js'),
        path.join(__dirname, p+'/index.js'),
    ];
}

async function exists(file) {
    try {
        const st = await fs.stat(file);
        return st.isFile();
    } catch (error) {
        if (typeof error === 'object' && 'code' in error && error.code === 'ENOENT') {
            return false;
        }
        throw error;
    }
}
