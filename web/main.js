import { api_v3, ED25519Key, Signer } from "accumulate.js";
import { Transaction, TransactionHeader } from "accumulate.js/lib/core";
import { Envelope } from "accumulate.js/lib/messaging";

// You need to import the Payload class for the type of transaction you want to
// make. Here we are building a SendTokens transaction.
import { SendTokens } from "accumulate.js/lib/core";

// Query some random account
const client = new api_v3.JsonRpcClient("https://kermit.accumulatenetwork.io/v3");
console.log(await client.query('test.acme/acmetokens'));
console.log(await client.query('test.acme', { queryType: 'directory', range: { } }));

// Generate a key. THIS WILL FAIL because it's a newly generated account so it
// necessarily won't have a balance. This is just a proof of concept.
const sender = await Signer.forLite(await ED25519Key.generate());

// Build the Payload
const recipient = await Signer.forLite(await ED25519Key.generate());
const amount = 10;
const body = new SendTokens({ to: [{ url: recipient.url.join("ACME"), amount: amount }] });
// Build the transaction header with the transaction principal
// and optionally a timestamp, memo or metadata.
const header = new TransactionHeader({ principal: sender.url.join("ACME") });

// Finally build the (unsigned yet) transaction
const tx = new Transaction({ body, header });

// Sign with a key pair or manually sign with custom key store, Ledger, etc
const sig = await sender.sign(tx, { timestamp: Date.now() });
const env = new Envelope({ transaction: [tx], signatures: [sig] });

// Validate asks the network to pretend to execute a transaction without
// actually executing. Use submit to actually execute.
console.log(await client.validate(env));