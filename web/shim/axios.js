export default {
    create(options) {
        return new AxiosInstance(options)
    }
}

export class AxiosInstance {
    constructor(options) {
        this.options = options
    }

    async post(url, request) {
        const r = await fetch(url, {
            method: 'post',
            headers: this.options.headers,
            body: JSON.stringify(request)
        })
        return {
            data: await r.json()
        }
    }
}