```shell
# Clone the repository, including submodules
git clone --recursive https://gitlab.com/defidevs/js-example.git

# Compile the SDK
./compile-sdk.sh

# Run the webserver
yarn install
node server.js

# Open http://localhost:3000 in your browser
```